from django.urls import path

from .views import ArtifactBundleAssembleView, ChunkUploadAPIView

urlpatterns = [
    path(
        "organizations/<slug:organization_slug>/chunk-upload/",
        ChunkUploadAPIView.as_view(),
        name="chunk-upload",
    ),
    path(
        "organizations/<slug:organization_slug>/artifactbundle/assemble/",
        ArtifactBundleAssembleView.as_view(),
        name="artifact-assemble",
    ),
]
